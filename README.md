# QaProgram.Unit.Testing



## O inicio
Este é um exemplo básico do funcionamento de testes unitários utilizando Javascript. O exemplo testes básicos e relatórios de cobertura de testes pra upload em ferramentas de análise estática como SonarQube.

## Novos testes podem ser adicionados seguindo os passos abaixo

##REQUERIDO

- [ ] [Novo teste] Duplique um teste existente adicionando a verificação de um novo idioma ainda não coberto pelo código
- [ ] [Execute os teste] Execute os testes executando o comando 
- [ ] [Verifique os testes falharem e a cobertura] Note que novo teste irá falhar pois não um trecho de código capaz de executar o esperado pelo teste
- [ ] [Desenvolva um trecho de código capaz de executar os resultados esperados pelo teste] Duplique um trecho de código existente e faça o ajuste para um novo idioma, aquele mesmo do teste do passo anterior >> VOCÊ ESTÁ FAZENDO TDD :)
- [ ] [Execute os testes novamente] Note todos seus testes passarem com sucesso :)

##OPCIONAL

- [ ] [Desenvolva um trecho de código adicionando o suporte a um novo idioma] Duplique um trecho de código existente e faça o ajuste para um novo idioma, aquele mesmo do teste do passo anterior >> VOCÊ NÃO ESTÁ FAZENDO TDD, MAS TUDO BEM :) -> NOTE A DIFERENÇA NO FLUXO
- [ ] [Execute os teste e veja a sua cobertura cair] Execute os testes executando o comando  e verifique que a cobertura caiu pois não foram criados novos testes para o trecho de código implementando
- [ ] [Novo teste] Duplique um teste existente adicionando a verificação do novo idioma implementado
- [ ] [Verifique que a cobertura subiu e todos os testes passam com sucesso] Note que agora temos uma cobertura de 100% novamente e os testes passam com sucesso