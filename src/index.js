const englishCode = "en-UK";

const frenchCode = "es-FS";

const portugueseCode = "pt-BR";

const hebrewCode = "hebrew";

function getAboutUsLink(language){

  switch (language.toLowerCase()){

  case englishCode.toLowerCase():

  return '/about-us';

  case frenchCode.toLowerCase():

  return '/-à propos de nous';

 case portugueseCode.toLowerCase():

    return '/sobre-nos';

case hebrewCode.toLowerCase():

    return '/עלינו';

  }

  return '';

}

module.exports = getAboutUsLink;