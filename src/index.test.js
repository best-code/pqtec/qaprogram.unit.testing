const getAboutUsLink = require("./index");

test("Retornar link sobre nos para idioma ingles", () => {

    expect(getAboutUsLink("en-UK")).toBe("/about-us");

});

test("Retornar link sobre nos para idioma portugues-br", () => {

    expect(getAboutUsLink("pt-BR")).toBe("/sobre-nos");

});

test("Retornar link sobre nos para idioma indefindo e nao mapeado", () => {

    expect(getAboutUsLink("pt-PT")).toBe("");

});


test("Retornar link sobre nos para idioma hebraico", () => {

    expect(getAboutUsLink("hebrew")).toBe("/עלינו");

});